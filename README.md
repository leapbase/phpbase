# phpbase

PHP restful api service

# Support Install

For MacOSX

```
brew install php@7.3

brew install composer
```

For Ubuntu

```
sudo apt-get install php

sudo apt-get install composer
```


# Git Setup

```
git config user.name $GITLAB_USER_NAME
git config user.email $GITLAB_USER_EMAIL

git config credential.helper 'cache --timeout=3600'
git config credential.helper store

git config --list
```


# Install Dependencies

```
composer install
```



